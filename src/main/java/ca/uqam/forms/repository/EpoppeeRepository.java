package ca.uqam.forms.repository;

import ca.uqam.forms.domain.Epoppee;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Epoppee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EpoppeeRepository extends MongoRepository<Epoppee, String> {
}
