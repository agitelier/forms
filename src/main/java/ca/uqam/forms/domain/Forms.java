package ca.uqam.forms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * A Forms.
 */
@Document(collection = "forms")
public class Forms implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("form_name")
    private String formName;

    @Field("date")
    private String date;

    @Field("nom")
    private String nom;

    @Field("reference")
    private String reference;

    @Field("description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormName() {
        return formName;
    }

    public Forms formName(String formName) {
        this.formName = formName;
        return this;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getDate() {
        return date;
    }

    public Forms date(String date) {
        this.date = date;
        return this;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNom() {
        return nom;
    }

    public Forms nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getReference() {
        return reference;
    }

    public Forms reference(String reference) {
        this.reference = reference;
        return this;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public Forms description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Forms)) {
            return false;
        }
        return id != null && id.equals(((Forms) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Forms{" +
            "id=" + getId() +
            ", formName='" + getFormName() + "'" +
            ", date='" + getDate() + "'" +
            ", nom='" + getNom() + "'" +
            ", reference='" + getReference() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
