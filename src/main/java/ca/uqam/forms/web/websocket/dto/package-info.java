/**
 * Data Access Objects used by WebSocket services.
 */
package ca.uqam.forms.web.websocket.dto;
