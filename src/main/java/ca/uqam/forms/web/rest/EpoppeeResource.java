package ca.uqam.forms.web.rest;

import ca.uqam.forms.domain.Epoppee;
import ca.uqam.forms.repository.EpoppeeRepository;
import ca.uqam.forms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ca.uqam.forms.domain.Epoppee}.
 */
@RestController
@RequestMapping("/api")
public class EpoppeeResource {

    private final Logger log = LoggerFactory.getLogger(EpoppeeResource.class);

    private static final String ENTITY_NAME = "epoppee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EpoppeeRepository epoppeeRepository;

    public EpoppeeResource(EpoppeeRepository epoppeeRepository) {
        this.epoppeeRepository = epoppeeRepository;
    }

    /**
     * {@code POST  /epoppees} : Create a new epoppee.
     *
     * @param epoppee the epoppee to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new epoppee, or with status {@code 400 (Bad Request)} if the epoppee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/epoppees")
    public ResponseEntity<Epoppee> createEpoppee(@RequestBody Epoppee epoppee) throws URISyntaxException {
        log.debug("REST request to save Epoppee : {}", epoppee);
        if (epoppee.getId() != null) {
            throw new BadRequestAlertException("A new epoppee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Epoppee result = epoppeeRepository.save(epoppee);
        return ResponseEntity.created(new URI("/api/epoppees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /epoppees} : Updates an existing epoppee.
     *
     * @param epoppee the epoppee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated epoppee,
     * or with status {@code 400 (Bad Request)} if the epoppee is not valid,
     * or with status {@code 500 (Internal Server Error)} if the epoppee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/epoppees")
    public ResponseEntity<Epoppee> updateEpoppee(@RequestBody Epoppee epoppee) throws URISyntaxException {
        log.debug("REST request to update Epoppee : {}", epoppee);
        if (epoppee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Epoppee result = epoppeeRepository.save(epoppee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, epoppee.getId()))
            .body(result);
    }

    /**
     * {@code GET  /epoppees} : get all the epoppees.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of epoppees in body.
     */
    @GetMapping("/epoppees")
    public List<Epoppee> getAllEpoppees() {
        log.debug("REST request to get all Epoppees");
        return epoppeeRepository.findAll();
    }

    /**
     * {@code GET  /epoppees/:id} : get the "id" epoppee.
     *
     * @param id the id of the epoppee to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the epoppee, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/epoppees/{id}")
    public ResponseEntity<Epoppee> getEpoppee(@PathVariable String id) {
        log.debug("REST request to get Epoppee : {}", id);
        Optional<Epoppee> epoppee = epoppeeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(epoppee);
    }

    /**
     * {@code DELETE  /epoppees/:id} : delete the "id" epoppee.
     *
     * @param id the id of the epoppee to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/epoppees/{id}")
    public ResponseEntity<Void> deleteEpoppee(@PathVariable String id) {
        log.debug("REST request to delete Epoppee : {}", id);
        epoppeeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
