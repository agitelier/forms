/**
 * View Models used by Spring MVC REST controllers.
 */
package ca.uqam.forms.web.rest.vm;
