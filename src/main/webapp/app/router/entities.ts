import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Forms = () => import('@/entities/forms/forms.vue');
// prettier-ignore
const FormsUpdate = () => import('@/entities/forms/forms-update.vue');
// prettier-ignore
const FormsDetails = () => import('@/entities/forms/forms-details.vue');
// prettier-ignore
const Epoppee = () => import('@/entities/epoppee/epoppee.vue');
// prettier-ignore
const EpoppeeUpdate = () => import('@/entities/epoppee/epoppee-update.vue');
// prettier-ignore
const EpoppeeDetails = () => import('@/entities/epoppee/epoppee-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/forms',
    name: 'Forms',
    component: Forms,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/forms/new',
    name: 'FormsCreate',
    component: FormsUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/forms/:formsId/edit',
    name: 'FormsEdit',
    component: FormsUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/forms/:formsId/view',
    name: 'FormsView',
    component: FormsDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/epoppee',
    name: 'Epoppee',
    component: Epoppee,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/epoppee/new',
    name: 'EpoppeeCreate',
    component: EpoppeeUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/epoppee/:epoppeeId/edit',
    name: 'EpoppeeEdit',
    component: EpoppeeUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/epoppee/:epoppeeId/view',
    name: 'EpoppeeView',
    component: EpoppeeDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
