export interface IEpoppee {
  id?: string;
}

export class Epoppee implements IEpoppee {
  constructor(public id?: string) {}
}
