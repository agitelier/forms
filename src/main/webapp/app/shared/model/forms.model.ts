export interface IForms {
  id?: string;
  formName?: string;
  date?: string;
  nom?: string;
  reference?: string;
  description?: string;
}

export class Forms implements IForms {
  constructor(
    public id?: string,
    public formName?: string,
    public date?: string,
    public nom?: string,
    public reference?: string,
    public description?: string
  ) {}
}
