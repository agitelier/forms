import Component from 'vue-class-component';
import { Inject, Vue } from 'vue-property-decorator';
import { format } from 'date-fns';
import { DATE_TIME_FORMAT } from '@/shared/date/filters';

type TRow = { id: number; title: string; content: string; lastModifiedBy: string; lastModifiedAt: string; createdAt: string };

const SAMPLE_DATA = {
  id: 1,
  title: 'Title1',
  content: 'test',
  lastModifiedBy: 'Admin',
  lastModifiedAt: format(new Date(), DATE_TIME_FORMAT),
  createdAt: format(new Date(), DATE_TIME_FORMAT),
};

@Component({})
export default class DynamicTable extends Vue {
  rows: TRow[] = [SAMPLE_DATA];

  public addRow(): void {
    if (this.rows.length) {
      const last = this.rows[this.rows.length - 1];
      this.rows.push({ ...last, id: last.id + 1 });
    } else {
      this.rows.push({ ...SAMPLE_DATA, id: SAMPLE_DATA.id + 1 });
    }
  }

  public deleteRow(id: number): void {
    this.rows = this.rows.filter(r => r.id !== id);
  }
}
