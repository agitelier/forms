import Component from 'vue-class-component';
import { Inject, Vue } from 'vue-property-decorator';
import formsUpdate from '@/entities/forms/forms-update.vue';
import LoginService from '@/account/login.service';
import DynamicTable from '@/shared/components/DynamicTable/dynamicTable.vue';

const components = {
  formsUpdate,
  DynamicTable,
};

@Component({
  components,
})
export default class Forms extends Vue {
  @Inject('loginService')
  private loginService: () => LoginService;

  currentRoute: string;

  created() {
    this.currentRoute = this.$route.params.formContent;
    console.log(this.authenticated);
  }

  public get title() {
    return this.forms.find(e => e.path === this.currentRoute).title;
  }

  public openLogin(): void {
    this.loginService().login();
  }

  public get authenticated(): boolean {
    return this.$store.getters.authenticated;
  }

  forms = [
    {
      id: 0,
      title: 'Fonctionalité',
      path: 'fonctionalite',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...',
    },
    {
      id: 1,
      title: 'Thème strategique',
      path: 'theme-strategique',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...',
    },
    {
      id: 2,
      title: 'Épopée',
      path: 'epopee',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...',
    },
    {
      id: 3,
      title: 'Toile du portfolio',
      path: 'toile-du-portfolio',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...',
    },
    {
      id: 4,
      title: 'Delete or update forms',
      path: 'Delete-update',
      details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...',
    },
  ];
}
