import { Component, Vue, Inject, Prop } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IForms, Forms } from '@/shared/model/forms.model';
import FormsService from './forms.service';

const validations: any = {
  forms: {
    formName: {},
    date: {},
    nom: {},
    reference: {},
    description: {},
  },
};

@Component({
  validations,
})
export default class FormsUpdate extends Vue {
  @Prop()
  title: string;

  @Inject('alertService') private alertService: () => AlertService;
  @Inject('formsService') private formsService: () => FormsService;
  public forms: IForms = new Forms();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.formsId) {
        vm.retrieveForms(to.params.formsId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.$v.forms.formName.$model = this.title;
  }

  public save(): void {
    this.isSaving = true;
    if (this.forms.id) {
      this.formsService()
        .update(this.forms)
        .then(param => {
          this.isSaving = false;
          this.$router.replace(this.$router.currentRoute.path);
          const message = this.$t('formsApp.forms.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.formsService()
        .create(this.forms)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          //this.$router.replace(this.$router.currentRoute.path);
          const message = this.$t('formsApp.forms.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveForms(formsId): void {
    this.formsService()
      .find(formsId)
      .then(res => {
        this.forms = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
