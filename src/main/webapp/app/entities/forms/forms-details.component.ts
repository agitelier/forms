import { Component, Vue, Inject } from 'vue-property-decorator';

import { IForms } from '@/shared/model/forms.model';
import FormsService from './forms.service';

@Component
export default class FormsDetails extends Vue {
  @Inject('formsService') private formsService: () => FormsService;
  public forms: IForms = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.formsId) {
        vm.retrieveForms(to.params.formsId);
      }
    });
  }

  public retrieveForms(formsId) {
    this.formsService()
      .find(formsId)
      .then(res => {
        this.forms = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
