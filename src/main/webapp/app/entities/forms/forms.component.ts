import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IForms } from '@/shared/model/forms.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import FormsService from './forms.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Forms extends mixins(AlertMixin) {
  @Inject('formsService') private formsService: () => FormsService;
  private removeId: string = null;

  public forms: IForms[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllFormss();
  }

  public clear(): void {
    this.retrieveAllFormss();
  }

  public retrieveAllFormss(): void {
    this.isFetching = true;

    this.formsService()
      .retrieve()
      .then(
        res => {
          this.forms = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IForms): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeForms(): void {
    this.formsService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('formsApp.forms.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllFormss();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
