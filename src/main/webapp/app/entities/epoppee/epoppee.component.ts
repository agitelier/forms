import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IEpoppee } from '@/shared/model/epoppee.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import EpoppeeService from './epoppee.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Epoppee extends mixins(AlertMixin) {
  @Inject('epoppeeService') private epoppeeService: () => EpoppeeService;
  private removeId: string = null;

  public epoppees: IEpoppee[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllEpoppees();
  }

  public clear(): void {
    this.retrieveAllEpoppees();
  }

  public retrieveAllEpoppees(): void {
    this.isFetching = true;

    this.epoppeeService()
      .retrieve()
      .then(
        res => {
          this.epoppees = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IEpoppee): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeEpoppee(): void {
    this.epoppeeService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('formsApp.epoppee.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllEpoppees();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
