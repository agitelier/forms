import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IEpoppee, Epoppee } from '@/shared/model/epoppee.model';
import EpoppeeService from './epoppee.service';

const validations: any = {
  epoppee: {},
};

@Component({
  validations,
})
export default class EpoppeeUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('epoppeeService') private epoppeeService: () => EpoppeeService;
  public epoppee: IEpoppee = new Epoppee();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.epoppeeId) {
        vm.retrieveEpoppee(to.params.epoppeeId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.epoppee.id) {
      this.epoppeeService()
        .update(this.epoppee)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('formsApp.epoppee.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.epoppeeService()
        .create(this.epoppee)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('formsApp.epoppee.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveEpoppee(epoppeeId): void {
    this.epoppeeService()
      .find(epoppeeId)
      .then(res => {
        this.epoppee = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
