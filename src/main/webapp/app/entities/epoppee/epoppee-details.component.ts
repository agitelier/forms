import { Component, Vue, Inject } from 'vue-property-decorator';

import { IEpoppee } from '@/shared/model/epoppee.model';
import EpoppeeService from './epoppee.service';

@Component
export default class EpoppeeDetails extends Vue {
  @Inject('epoppeeService') private epoppeeService: () => EpoppeeService;
  public epoppee: IEpoppee = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.epoppeeId) {
        vm.retrieveEpoppee(to.params.epoppeeId);
      }
    });
  }

  public retrieveEpoppee(epoppeeId) {
    this.epoppeeService()
      .find(epoppeeId)
      .then(res => {
        this.epoppee = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
