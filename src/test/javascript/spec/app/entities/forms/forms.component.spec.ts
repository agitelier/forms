/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import FormsComponent from '@/entities/forms/forms.vue';
import FormsClass from '@/entities/forms/forms.component';
import FormsService from '@/entities/forms/forms.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Forms Management Component', () => {
    let wrapper: Wrapper<FormsClass>;
    let comp: FormsClass;
    let formsServiceStub: SinonStubbedInstance<FormsService>;

    beforeEach(() => {
      formsServiceStub = sinon.createStubInstance<FormsService>(FormsService);
      formsServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<FormsClass>(FormsComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          formsService: () => formsServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      formsServiceStub.retrieve.resolves({ headers: {}, data: [{ id: '123' }] });

      // WHEN
      comp.retrieveAllFormss();
      await comp.$nextTick();

      // THEN
      expect(formsServiceStub.retrieve.called).toBeTruthy();
      expect(comp.forms[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      formsServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: '123' });
      comp.removeForms();
      await comp.$nextTick();

      // THEN
      expect(formsServiceStub.delete.called).toBeTruthy();
      expect(formsServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
