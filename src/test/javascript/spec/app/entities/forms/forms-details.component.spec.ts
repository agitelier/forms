/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import FormsDetailComponent from '@/entities/forms/forms-details.vue';
import FormsClass from '@/entities/forms/forms-details.component';
import FormsService from '@/entities/forms/forms.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Forms Management Detail Component', () => {
    let wrapper: Wrapper<FormsClass>;
    let comp: FormsClass;
    let formsServiceStub: SinonStubbedInstance<FormsService>;

    beforeEach(() => {
      formsServiceStub = sinon.createStubInstance<FormsService>(FormsService);

      wrapper = shallowMount<FormsClass>(FormsDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { formsService: () => formsServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundForms = { id: '123' };
        formsServiceStub.find.resolves(foundForms);

        // WHEN
        comp.retrieveForms('123');
        await comp.$nextTick();

        // THEN
        expect(comp.forms).toBe(foundForms);
      });
    });
  });
});
