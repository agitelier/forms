/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import EpoppeeDetailComponent from '@/entities/epoppee/epoppee-details.vue';
import EpoppeeClass from '@/entities/epoppee/epoppee-details.component';
import EpoppeeService from '@/entities/epoppee/epoppee.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Epoppee Management Detail Component', () => {
    let wrapper: Wrapper<EpoppeeClass>;
    let comp: EpoppeeClass;
    let epoppeeServiceStub: SinonStubbedInstance<EpoppeeService>;

    beforeEach(() => {
      epoppeeServiceStub = sinon.createStubInstance<EpoppeeService>(EpoppeeService);

      wrapper = shallowMount<EpoppeeClass>(EpoppeeDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { epoppeeService: () => epoppeeServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundEpoppee = { id: '123' };
        epoppeeServiceStub.find.resolves(foundEpoppee);

        // WHEN
        comp.retrieveEpoppee('123');
        await comp.$nextTick();

        // THEN
        expect(comp.epoppee).toBe(foundEpoppee);
      });
    });
  });
});
