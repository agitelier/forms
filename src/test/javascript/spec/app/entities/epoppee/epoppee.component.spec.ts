/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import EpoppeeComponent from '@/entities/epoppee/epoppee.vue';
import EpoppeeClass from '@/entities/epoppee/epoppee.component';
import EpoppeeService from '@/entities/epoppee/epoppee.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Epoppee Management Component', () => {
    let wrapper: Wrapper<EpoppeeClass>;
    let comp: EpoppeeClass;
    let epoppeeServiceStub: SinonStubbedInstance<EpoppeeService>;

    beforeEach(() => {
      epoppeeServiceStub = sinon.createStubInstance<EpoppeeService>(EpoppeeService);
      epoppeeServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<EpoppeeClass>(EpoppeeComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          epoppeeService: () => epoppeeServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      epoppeeServiceStub.retrieve.resolves({ headers: {}, data: [{ id: '123' }] });

      // WHEN
      comp.retrieveAllEpoppees();
      await comp.$nextTick();

      // THEN
      expect(epoppeeServiceStub.retrieve.called).toBeTruthy();
      expect(comp.epoppees[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      epoppeeServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: '123' });
      comp.removeEpoppee();
      await comp.$nextTick();

      // THEN
      expect(epoppeeServiceStub.delete.called).toBeTruthy();
      expect(epoppeeServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
