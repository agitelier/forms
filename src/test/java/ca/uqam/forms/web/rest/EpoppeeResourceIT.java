package ca.uqam.forms.web.rest;

import ca.uqam.forms.FormsApp;
import ca.uqam.forms.config.TestSecurityConfiguration;
import ca.uqam.forms.domain.Epoppee;
import ca.uqam.forms.repository.EpoppeeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EpoppeeResource} REST controller.
 */
@SpringBootTest(classes = { FormsApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class EpoppeeResourceIT {

    @Autowired
    private EpoppeeRepository epoppeeRepository;

    @Autowired
    private MockMvc restEpoppeeMockMvc;

    private Epoppee epoppee;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Epoppee createEntity() {
        Epoppee epoppee = new Epoppee();
        return epoppee;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Epoppee createUpdatedEntity() {
        Epoppee epoppee = new Epoppee();
        return epoppee;
    }

    @BeforeEach
    public void initTest() {
        epoppeeRepository.deleteAll();
        epoppee = createEntity();
    }

    @Test
    public void createEpoppee() throws Exception {
        int databaseSizeBeforeCreate = epoppeeRepository.findAll().size();
        // Create the Epoppee
        restEpoppeeMockMvc.perform(post("/api/epoppees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(epoppee)))
            .andExpect(status().isCreated());

        // Validate the Epoppee in the database
        List<Epoppee> epoppeeList = epoppeeRepository.findAll();
        assertThat(epoppeeList).hasSize(databaseSizeBeforeCreate + 1);
        Epoppee testEpoppee = epoppeeList.get(epoppeeList.size() - 1);
    }

    @Test
    public void createEpoppeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = epoppeeRepository.findAll().size();

        // Create the Epoppee with an existing ID
        epoppee.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restEpoppeeMockMvc.perform(post("/api/epoppees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(epoppee)))
            .andExpect(status().isBadRequest());

        // Validate the Epoppee in the database
        List<Epoppee> epoppeeList = epoppeeRepository.findAll();
        assertThat(epoppeeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllEpoppees() throws Exception {
        // Initialize the database
        epoppeeRepository.save(epoppee);

        // Get all the epoppeeList
        restEpoppeeMockMvc.perform(get("/api/epoppees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(epoppee.getId())));
    }
    
    @Test
    public void getEpoppee() throws Exception {
        // Initialize the database
        epoppeeRepository.save(epoppee);

        // Get the epoppee
        restEpoppeeMockMvc.perform(get("/api/epoppees/{id}", epoppee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(epoppee.getId()));
    }
    @Test
    public void getNonExistingEpoppee() throws Exception {
        // Get the epoppee
        restEpoppeeMockMvc.perform(get("/api/epoppees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateEpoppee() throws Exception {
        // Initialize the database
        epoppeeRepository.save(epoppee);

        int databaseSizeBeforeUpdate = epoppeeRepository.findAll().size();

        // Update the epoppee
        Epoppee updatedEpoppee = epoppeeRepository.findById(epoppee.getId()).get();

        restEpoppeeMockMvc.perform(put("/api/epoppees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEpoppee)))
            .andExpect(status().isOk());

        // Validate the Epoppee in the database
        List<Epoppee> epoppeeList = epoppeeRepository.findAll();
        assertThat(epoppeeList).hasSize(databaseSizeBeforeUpdate);
        Epoppee testEpoppee = epoppeeList.get(epoppeeList.size() - 1);
    }

    @Test
    public void updateNonExistingEpoppee() throws Exception {
        int databaseSizeBeforeUpdate = epoppeeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEpoppeeMockMvc.perform(put("/api/epoppees").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(epoppee)))
            .andExpect(status().isBadRequest());

        // Validate the Epoppee in the database
        List<Epoppee> epoppeeList = epoppeeRepository.findAll();
        assertThat(epoppeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteEpoppee() throws Exception {
        // Initialize the database
        epoppeeRepository.save(epoppee);

        int databaseSizeBeforeDelete = epoppeeRepository.findAll().size();

        // Delete the epoppee
        restEpoppeeMockMvc.perform(delete("/api/epoppees/{id}", epoppee.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Epoppee> epoppeeList = epoppeeRepository.findAll();
        assertThat(epoppeeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
