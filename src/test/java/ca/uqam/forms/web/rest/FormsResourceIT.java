package ca.uqam.forms.web.rest;

import ca.uqam.forms.FormsApp;
import ca.uqam.forms.config.TestSecurityConfiguration;
import ca.uqam.forms.domain.Forms;
import ca.uqam.forms.repository.FormsRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FormsResource} REST controller.
 */
@SpringBootTest(classes = { FormsApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class FormsResourceIT {

    private static final String DEFAULT_FORM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FORM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DATE = "AAAAAAAAAA";
    private static final String UPDATED_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private FormsRepository formsRepository;

    @Autowired
    private MockMvc restFormsMockMvc;

    private Forms forms;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Forms createEntity() {
        Forms forms = new Forms()
            .formName(DEFAULT_FORM_NAME)
            .date(DEFAULT_DATE)
            .nom(DEFAULT_NOM)
            .reference(DEFAULT_REFERENCE)
            .description(DEFAULT_DESCRIPTION);
        return forms;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Forms createUpdatedEntity() {
        Forms forms = new Forms()
            .formName(UPDATED_FORM_NAME)
            .date(UPDATED_DATE)
            .nom(UPDATED_NOM)
            .reference(UPDATED_REFERENCE)
            .description(UPDATED_DESCRIPTION);
        return forms;
    }

    @BeforeEach
    public void initTest() {
        formsRepository.deleteAll();
        forms = createEntity();
    }

    @Test
    public void createForms() throws Exception {
        int databaseSizeBeforeCreate = formsRepository.findAll().size();
        // Create the Forms
        restFormsMockMvc.perform(post("/api/forms").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(forms)))
            .andExpect(status().isCreated());

        // Validate the Forms in the database
        List<Forms> formsList = formsRepository.findAll();
        assertThat(formsList).hasSize(databaseSizeBeforeCreate + 1);
        Forms testForms = formsList.get(formsList.size() - 1);
        assertThat(testForms.getFormName()).isEqualTo(DEFAULT_FORM_NAME);
        assertThat(testForms.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testForms.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testForms.getReference()).isEqualTo(DEFAULT_REFERENCE);
        assertThat(testForms.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createFormsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = formsRepository.findAll().size();

        // Create the Forms with an existing ID
        forms.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restFormsMockMvc.perform(post("/api/forms").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(forms)))
            .andExpect(status().isBadRequest());

        // Validate the Forms in the database
        List<Forms> formsList = formsRepository.findAll();
        assertThat(formsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllForms() throws Exception {
        // Initialize the database
        formsRepository.save(forms);

        // Get all the formsList
        restFormsMockMvc.perform(get("/api/forms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(forms.getId())))
            .andExpect(jsonPath("$.[*].formName").value(hasItem(DEFAULT_FORM_NAME)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].reference").value(hasItem(DEFAULT_REFERENCE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    public void getForms() throws Exception {
        // Initialize the database
        formsRepository.save(forms);

        // Get the forms
        restFormsMockMvc.perform(get("/api/forms/{id}", forms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(forms.getId()))
            .andExpect(jsonPath("$.formName").value(DEFAULT_FORM_NAME))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.reference").value(DEFAULT_REFERENCE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    public void getNonExistingForms() throws Exception {
        // Get the forms
        restFormsMockMvc.perform(get("/api/forms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateForms() throws Exception {
        // Initialize the database
        formsRepository.save(forms);

        int databaseSizeBeforeUpdate = formsRepository.findAll().size();

        // Update the forms
        Forms updatedForms = formsRepository.findById(forms.getId()).get();
        updatedForms
            .formName(UPDATED_FORM_NAME)
            .date(UPDATED_DATE)
            .nom(UPDATED_NOM)
            .reference(UPDATED_REFERENCE)
            .description(UPDATED_DESCRIPTION);

        restFormsMockMvc.perform(put("/api/forms").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedForms)))
            .andExpect(status().isOk());

        // Validate the Forms in the database
        List<Forms> formsList = formsRepository.findAll();
        assertThat(formsList).hasSize(databaseSizeBeforeUpdate);
        Forms testForms = formsList.get(formsList.size() - 1);
        assertThat(testForms.getFormName()).isEqualTo(UPDATED_FORM_NAME);
        assertThat(testForms.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testForms.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testForms.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testForms.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void updateNonExistingForms() throws Exception {
        int databaseSizeBeforeUpdate = formsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFormsMockMvc.perform(put("/api/forms").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(forms)))
            .andExpect(status().isBadRequest());

        // Validate the Forms in the database
        List<Forms> formsList = formsRepository.findAll();
        assertThat(formsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteForms() throws Exception {
        // Initialize the database
        formsRepository.save(forms);

        int databaseSizeBeforeDelete = formsRepository.findAll().size();

        // Delete the forms
        restFormsMockMvc.perform(delete("/api/forms/{id}", forms.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Forms> formsList = formsRepository.findAll();
        assertThat(formsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
