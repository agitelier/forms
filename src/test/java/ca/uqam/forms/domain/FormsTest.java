package ca.uqam.forms.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ca.uqam.forms.web.rest.TestUtil;

public class FormsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Forms.class);
        Forms forms1 = new Forms();
        forms1.setId("id1");
        Forms forms2 = new Forms();
        forms2.setId(forms1.getId());
        assertThat(forms1).isEqualTo(forms2);
        forms2.setId("id2");
        assertThat(forms1).isNotEqualTo(forms2);
        forms1.setId(null);
        assertThat(forms1).isNotEqualTo(forms2);
    }
}
