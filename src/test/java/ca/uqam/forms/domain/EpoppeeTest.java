package ca.uqam.forms.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ca.uqam.forms.web.rest.TestUtil;

public class EpoppeeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Epoppee.class);
        Epoppee epoppee1 = new Epoppee();
        epoppee1.setId("id1");
        Epoppee epoppee2 = new Epoppee();
        epoppee2.setId(epoppee1.getId());
        assertThat(epoppee1).isEqualTo(epoppee2);
        epoppee2.setId("id2");
        assertThat(epoppee1).isNotEqualTo(epoppee2);
        epoppee1.setId(null);
        assertThat(epoppee1).isNotEqualTo(epoppee2);
    }
}
